// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.24;

import {Clones} from "@openzeppelin/contracts/proxy/Clones.sol";

import {Ownable} from "@openzeppelin/contracts/access/Ownable.sol";

import {GovernorPool} from "./abstract/GovernorPool.sol";
import {QueuePool} from "./QueuePool.sol";
import {RandomPool} from "./RandomPool.sol";
import {ReferralCenter} from "./ReferralCenter.sol";

contract Alchemestry is Ownable(msg.sender) {
    uint256 public constant PERCENTAGE_BPS = 100;

    uint256 public constant DEVELOPERS_TABLE_FEE = 10 * PERCENTAGE_BPS;
    uint256 public constant REFERRAL_TABLE_FEE = 15 * PERCENTAGE_BPS;
    uint256 public constant TABLE_FEE_DIVIDER = 100 * PERCENTAGE_BPS;

    address public immutable referralCenter;

    address public immutable queuePool;
    address public immutable randomPool;

    uint256 public immutable poolLevels;
    uint256 public immutable poolStart;
    uint256 public immutable poolLevelDuration;

    receive() external payable {}

    constructor(
        address _queuePoolMaster,
        address _randomPoolMaster,
        address _referralCenterMaster,
        address _vrfConsumer,
        uint256 _poolStart,
        uint256 _poolLevelDuration,
        uint256 _poolLevels
    ) {
        // TODO: move to verifyAddress function
        require(_queuePoolMaster != address(0), "ALCH: invalid address");
        require(_randomPoolMaster != address(0), "ALCH: invalid address");
        require(_referralCenterMaster != address(0), "ALCH: invalid address");

        require(_poolStart >= block.timestamp, "ALCH: invalid start");
        require(_poolLevelDuration > 0, "ALCH: invalid duration");
        require(_poolLevels > 0, "ALCH: invalid pool levels");

        address queue = Clones.clone(_queuePoolMaster);
        address random = Clones.clone(_randomPoolMaster);
        address _referralCenter = Clones.clone(_referralCenterMaster);

        address payable payableThis = payable(address(this));

        QueuePool(queue).initialize(owner(), payableThis);
        RandomPool(random).initialize(owner(), _vrfConsumer, payableThis);
        ReferralCenter(_referralCenter).initialize(payableThis, queue, random);

        referralCenter = _referralCenter;

        poolStart = _poolStart;
        poolLevelDuration = _poolLevelDuration;
        poolLevels = _poolLevels;

        queuePool = queue;
        randomPool = random;
    }

    function collectGovernanceFees(address collectTo) external onlyOwner {
        uint256 feeCollected;

        feeCollected += GovernorPool(queuePool).claimGovernanceFees(collectTo);
        feeCollected += GovernorPool(randomPool).claimGovernanceFees(collectTo);

        require(feeCollected > 0, "ALCH: nothing to collect");
    }

    function getCurrentActiveLevel() external view virtual returns (uint256) {
        return getActiveLevel(block.timestamp);
    }

    function getLevelStartTimestamp(
        uint256 level
    ) external view returns (uint256 start) {
        if (level == 0 || level > poolLevels) return 0;

        start += poolStart;

        for (uint256 i = level; i > 1; i--) {
            start += getPoolLevelDuration(i);
        }
    }

    function calculateFees(
        uint256 amount,
        uint256 tablePrice,
        address referral
    )
        external
        pure
        returns (
            uint256 globalFee,
            uint256 governanceFee,
            uint256 referralFee,
            uint256 singleTablePriceWithoutFees
        )
    {
        globalFee = (tablePrice * totalTableFee()) / TABLE_FEE_DIVIDER;

        if (referral != address(0)) {
            referralFee = (tablePrice * REFERRAL_TABLE_FEE) / TABLE_FEE_DIVIDER;
        }

        governanceFee = globalFee - referralFee;
        singleTablePriceWithoutFees = tablePrice - globalFee;

        globalFee *= amount;
        governanceFee *= amount;
        referralFee *= amount;
    }

    function getPoolLevelDuration(uint256) public view returns (uint256) {
        return poolLevelDuration;
    }

    function getActiveLevel(uint256 timestamp) public view returns (uint256) {
        if (timestamp < poolStart) return 0;

        uint256 t = poolStart;

        for (uint256 i = 1; i <= poolLevels; i++) {
            t += getPoolLevelDuration(i);
            if (timestamp < t) return i;
        }

        return 0;
    }

    function queuePoolUserTablesCount(
        uint256 level,
        address user
    ) public view returns (uint256) {
        return QueuePool(queuePool).userTablesCount(level, user);
    }

    function totalTableFee() public pure returns (uint256) {
        return DEVELOPERS_TABLE_FEE + REFERRAL_TABLE_FEE;
    }
}
