// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.24;

import {IPool} from "./interfaces/IPool.sol";
import {TableQueueLibrary} from "./libraries/TableQueueLibrary.sol";
import {Alchemestry} from "./Alchemestry.sol";
import {ReferralCenter} from "./ReferralCenter.sol";

import {GovernorPool} from "./abstract/GovernorPool.sol";

contract QueuePool is GovernorPool, IPool {
    using TableQueueLibrary for TableQueueLibrary.TableQueue;

    struct BuyTablesMultInput {
        uint256 level;
        uint256 amount;
        uint256 value;
        address referral;
    }

    mapping(uint256 => TableQueueLibrary.TableQueue) private _levelQueue;

    function initialize(
        address _owner,
        address payable _alchemestry
    ) external initializer {
        __GovernorPool_init(_owner, _alchemestry);

        // FIXME: ????
        _levelQueue[0].intervalsQueue._head = type(uint256).max;
        _levelQueue[0].intervalsQueue._tail = type(uint256).max;

        _levelQueue[1].intervalsQueue._head = type(uint256).max;
        _levelQueue[1].intervalsQueue._tail = type(uint256).max;

        _levelQueue[2].intervalsQueue._head = type(uint256).max;
        _levelQueue[2].intervalsQueue._tail = type(uint256).max;

        _levelQueue[3].intervalsQueue._head = type(uint256).max;
        _levelQueue[3].intervalsQueue._tail = type(uint256).max;

        _levelQueue[4].intervalsQueue._head = type(uint256).max;
        _levelQueue[4].intervalsQueue._tail = type(uint256).max;
    }

    function buyTables(
        uint256 level,
        uint256 amount,
        address referral
    ) external payable {
        _buyTables(msg.sender, level, amount, msg.value, referral);
    }

    function buyTablesMult(
        BuyTablesMultInput[] memory buyInfo
    ) external payable {
        require(buyInfo.length > 0, "Queue: invalid payload");

        uint256 totalValue;

        for (uint256 i; i < buyInfo.length; i++) {
            totalValue += buyInfo[i].value;
            _buyTables(
                msg.sender,
                buyInfo[i].level,
                buyInfo[i].amount,
                buyInfo[i].value,
                buyInfo[i].referral
            );
        }

        require(msg.value >= totalValue, "Queue: insufficient value");
    }

    function claimTableRewards(
        uint256 level,
        address claimFor
    ) public returns (uint256 claimed) {
        claimed = _levelQueue[level].claim(claimFor);

        payable(claimFor).transfer(claimed);
    }

    function claimTableRewardsAll(
        address claimFor
    ) public returns (uint256 claimed) {
        for (uint256 level = getCurrentActiveLevel(); level > 0; level--) {
            claimed += _levelQueue[level].claim(claimFor);
        }

        payable(claimFor).transfer(claimed);
    }

    function userTablesCount(
        uint256 _level,
        address _user
    ) public view returns (uint256) {
        return _levelQueue[_level].userInfos[_user].tablesCount;
    }

    function userLevelInfo(
        uint256 _level,
        address _user
    ) public view returns (TableQueueLibrary.QueueUserInfo memory) {
        return _levelQueue[_level].userInfos[_user];
    }

    function getPoolLevelDuration(uint256) public view returns (uint256) {
        return alchemestry.getPoolLevelDuration(0);
    }

    function getCurrentActiveLevel() public view returns (uint256) {
        return alchemestry.getCurrentActiveLevel();
    }

    function getTablePriceForLevel(
        uint256 level
    ) public pure returns (uint256) {
        if (level == 1) return 0.15 * (10 ** 18);
        else if (level == 2) return 0.30 * (10 ** 18);
        else if (level == 3) return 0.50 * (10 ** 18);
        else revert("Queue: invalid level id");
    }

    function getTableFulFillForLevel(
        uint256 level
    ) public pure returns (uint256) {
        uint256 price = getTablePriceForLevel(level);
        return price + price / 2;
    }

    function getMaxTablesPerTransactionForLevel(
        uint256
    ) public pure returns (uint256) {
        return 10;
    }

    function calculateFees(
        uint256 level,
        uint256 amount,
        address referral
    )
        public
        view
        returns (
            uint256 globalFee,
            uint256 governanceFee,
            uint256 referralFee,
            uint256 singleTablePriceWithoutFees
        )
    {
        uint256 tablePrice = getTablePriceForLevel(level);
        return alchemestry.calculateFees(amount, tablePrice, referral);
    }

    function _buyTables(
        address user,
        uint256 level,
        uint256 amount,
        uint256 msgValue,
        address referral
    ) private {
        // TODO: add event emit
        _verifyBuyTables(user, level, amount, msgValue);

        if (referral != address(0) && userTablesCount(level, referral) == 0) {
            referral = address(0);
        }

        // stack is to deep prevention
        uint256 _level = level;

        (
            ,
            uint256 governanceFee,
            uint256 referrerFee,
            uint256 singleTablePriceWithoutFees
        ) = calculateFees(_level, amount, referral);

        uint256 fillRemainer = _levelQueue[_level].addMult(
            user,
            amount,
            singleTablePriceWithoutFees,
            getTableFulFillForLevel(_level)
        );

        addReferralRewards(user, referrerFee);

        if (fillRemainer != 0) {
            governanceFee += fillRemainer;
        }

        collectGovernanceFee(governanceFee);
    }

    function _verifyBuyTables(
        address user,
        uint256 level,
        uint256 amount,
        uint256 msgValue
    ) private view {
        require(
            block.timestamp >= alchemestry.poolStart(),
            "Queue: pool is not started"
        );
        require(amount > 0 && level > 0, "Queue: invalid amount/level");
        require(
            amount <= getMaxTablesPerTransactionForLevel(level),
            "Queue: max tables exceeded"
        );
        require(
            msgValue >= getTablePriceForLevel(level) * amount,
            "Queue: insufficient value"
        );

        uint256 currentLevel = getCurrentActiveLevel();

        require(level <= currentLevel, "Queue: level > current level");

        if (level < currentLevel) {
            require(
                amount == 1 && userTablesCount(level, user) == 0,
                "Queue: prev level exceeded"
            );
        }

        if (level != 1) {
            _verifyPrevLevelTables(user, level - 1);
        }
    }

    function _verifyPrevLevelTables(address user, uint256 level) private view {
        require(
            userTablesCount(level, user) >= 1,
            "Queue: insufficient level tables"
        );

        if (level != 1) {
            _verifyPrevLevelTables(user, level - 1);
        }
    }
}
