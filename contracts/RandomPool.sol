// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.24;

import {AutomationCompatibleInterface} from "@chainlink/contracts/src/v0.8/automation/interfaces/AutomationCompatibleInterface.sol";

import {TableRandomPoolLibrary} from "./libraries/TableRandomPoolLibrary.sol";
import {RandomStatusLibrary} from "./libraries/RandomStatusLibrary.sol";

import {GovernorPool} from "./abstract/GovernorPool.sol";
import {IPool} from "./interfaces/IPool.sol";

import {VRFConsumer} from "./vrf/VRFConsumer.sol";
import {Alchemestry} from "./Alchemestry.sol";

contract RandomPool is GovernorPool, AutomationCompatibleInterface, IPool {
    using TableRandomPoolLibrary for TableRandomPoolLibrary.RandomPoolLevel;
    using RandomStatusLibrary for RandomStatusLibrary.RandomStatus;

    address public vrfConsumer;

    /// @dev level => TableRandomPool
    mapping(uint256 => TableRandomPoolLibrary.RandomPoolLevel)
        private _levelPool;

    event BuyTables(
        address indexed user,
        address indexed referral,
        uint256 indexed level,
        uint256 amount
    );

    event NewReferral(
        address indexed user,
        address indexed referrer,
        uint256 referrerFee
    );

    event ClaimRewards(
        address indexed user,
        uint256 indexed level,
        uint256 indexed claimedAmount
    );

    event PerformUpkeep(
        address indexed caller,
        uint256 indexed levelToFullfill,
        uint256 wordsRequested
    );

    event FulfillPool(
        uint256 indexed level,
        bool indexed isFulfilled,
        uint256 leftToFulfill
    );

    event FulfillTable(address indexed user, uint256 indexed level);

    function initialize(
        address _owner,
        address _vrfConsumer,
        address payable _alchemestry
    ) external initializer {
        __GovernorPool_init(_owner, _alchemestry);

        require(_vrfConsumer != address(0), "Random: invalid _vrfConsumer");

        vrfConsumer = _vrfConsumer;
    }

    function buyTables(
        uint256 level,
        uint256 amount,
        address referral
    ) external payable {
        return _buyTables(msg.sender, level, amount, msg.value, referral);
    }

    function claimTableRewards(
        uint256 level,
        address claimFor
    ) external returns (uint256 claimed) {
        claimed = _claimRewards(level, claimFor);
        payable(claimFor).transfer(claimed);
    }

    function claimTableRewardsAll(
        address claimFor
    ) external returns (uint256 claimed) {
        for (uint256 level = getCurrentActiveLevel() - 1; level > 0; level--) {
            claimed += _claimRewards(level, claimFor);
        }

        payable(claimFor).transfer(claimed);
    }

    function performUpkeep(bytes calldata /*performData*/) external override {
        uint256 currentLevel = getCurrentActiveLevel();

        require(currentLevel != 0, "Random: invalid level");

        uint256 levelToFill;

        for (levelToFill = 1; levelToFill < currentLevel; levelToFill++) {
            if (
                _levelPool[levelToFill].pool.randomStatus.isFilled ||
                _levelPool[levelToFill].pool.randomStatus.isLastRequestPending
            ) continue;
            break;
        }

        require(levelToFill != currentLevel, "Random: nothing to fill");

        uint256 wordsToRequest = _levelPool[levelToFill]
            .pool
            .randomStatus
            .leftToFill(calculateTotalRandomWinnersForLevel(levelToFill));

        uint256 maxWordsToRequest = getRandomFillTablesPerTransaction();

        if (wordsToRequest > maxWordsToRequest) {
            wordsToRequest = maxWordsToRequest;
        }

        emit PerformUpkeep(msg.sender, levelToFill, wordsToRequest);

        if (wordsToRequest == 0) {
            uint256[] memory arr = new uint256[](0);

            // nothing to request from VRF, pool should be marked as fullfiled
            _fillRandom(arr, levelToFill, 0);
            return;
        }
        uint256 requestId = VRFConsumer(vrfConsumer).requestRandomWords(
            uint32(wordsToRequest),
            levelToFill
        );

        _levelPool[levelToFill].pool.randomStatus.lastRequestId = requestId;
        _levelPool[levelToFill].pool.randomStatus.isLastRequestPending = true;
    }

    function fillRandom(
        uint256[] calldata wonIds,
        uint256 level,
        uint256 requestId
    ) external {
        require(msg.sender == vrfConsumer, "Random: sender not vrf");
        require(
            _levelPool[level].pool.randomStatus.lastRequestId == requestId,
            "Random: invalid request id"
        );

        _fillRandom(wonIds, level, requestId);
    }

    function checkUpkeep(
        bytes calldata /* checkData */
    ) external override returns (bool upkeepNeeded, bytes memory performData) {
        uint256 currentLevel = getCurrentActiveLevel();

        if (currentLevel == 0) {
            return (false, "");
        }

        uint256 levelToFill;

        for (levelToFill = 1; levelToFill < currentLevel; levelToFill++) {
            if (_levelPool[levelToFill].pool.randomStatus.isFilled) continue;
            break;
        }

        if (levelToFill == currentLevel) {
            return (false, "");
        }

        return (true, "");
    }

    function getTablePriceForLevel(
        uint256 level
    ) public pure returns (uint256) {
        if (level == 1) return 0.15 * (10 ** 18);
        else if (level == 2) return 0.30 * (10 ** 18);
        else if (level == 3) return 0.50 * (10 ** 18);
        else revert("Random: invalid level id");
    }

    function getMaxTablesPerTransactionForLevel(
        uint256
    ) public pure returns (uint256) {
        return 10;
    }

    function userTablesCount(
        uint256 _level,
        address _user
    ) public view returns (uint256) {
        return _levelPool[_level].userInfos[_user].tablesCount;
    }

    function userLevelInfo(
        uint256 _level,
        address _user
    ) public view returns (TableRandomPoolLibrary.RandomPoolUserInfo memory) {
        return _levelPool[_level].userInfos[_user];
    }

    function poolInfo(
        uint256 _level
    )
        external
        view
        returns (
            uint256 tablesCount,
            uint256 poolSize,
            uint256 claimed,
            bool isPoolManuallyFilled
        )
    {
        TableRandomPoolLibrary.RandomPoolInfo storage pool = _levelPool[_level]
            .pool;

        tablesCount = pool.tables.length;
        poolSize = pool.poolSize;
        claimed = pool.claimed;
        isPoolManuallyFilled = pool.isPoolManuallyFilled;
    }

    function calculateFees(
        uint256 level,
        uint256 amount,
        address referral
    )
        public
        view
        returns (
            uint256 globalFee,
            uint256 governanceFee,
            uint256 referrerFee,
            uint256 singleTablePriceWithoutFees
        )
    {
        uint256 tablePrice = getTablePriceForLevel(level);
        return alchemestry.calculateFees(amount, tablePrice, referral);
    }

    function getTableFulFillForLevel(
        uint256 level
    ) public pure returns (uint256) {
        uint256 price = getTablePriceForLevel(level);
        return price + price / 2;
    }

    function getRandomFillTablesPerTransaction()
        public
        view
        virtual
        returns (uint256)
    {
        return 35;
    }

    function getPoolLevelDuration(
        uint256 _level
    ) public view returns (uint256) {
        return alchemestry.getPoolLevelDuration(_level);
    }

    function getCurrentActiveLevel() public view returns (uint256) {
        return alchemestry.getCurrentActiveLevel();
    }

    function calculateTotalRandomWinnersForLevel(
        uint256 level
    ) public view returns (uint256) {
        return
            _levelPool[level].calculateTotalWinners(
                getTableFulFillForLevel(level)
            );
    }

    function _buyTables(
        address user,
        uint256 level,
        uint256 amount,
        uint256 msgValue,
        address referrer
    ) private {
        _verifyBuyTables(user, level, amount, msgValue);

        (
            ,
            uint256 governanceFee,
            uint256 referrerFee,
            uint256 tablePrice
        ) = calculateFees(level, amount, referrer);

        _levelPool[level].addTables(
            user,
            amount,
            tablePrice,
            getTableFulFillForLevel(level)
        );

        addReferralRewards(user, referrerFee);

        collectGovernanceFee(governanceFee);

        emit BuyTables(user, referrer, level, amount);

        if (referrer != address(0)) {
            emit NewReferral(user, referrer, referrerFee);
        }
    }

    function _claimRewards(
        uint256 level,
        address claimFor
    ) private returns (uint256 toClaim) {
        toClaim = _levelPool[level].claimRewards(
            claimFor,
            getTableFulFillForLevel(level),
            true
        );

        emit ClaimRewards(claimFor, level, toClaim);
    }

    function _fillRandom(
        uint256[] memory words,
        uint256 level,
        uint256 requestId
    ) private {
        uint256 leftToFulfill = _levelPool[level].fillRandom(
            words,
            calculateTotalRandomWinnersForLevel(level),
            requestId
        );

        emit FulfillPool(level, leftToFulfill == 0, leftToFulfill);
    }

    function _verifyBuyTables(
        address user,
        uint256 level,
        uint256 amount,
        uint256 msgValue
    ) private view {
        uint256 currentLevel = getCurrentActiveLevel();

        require(amount > 0, "Random: invalid amount");
        require(currentLevel > 0, "Random: pool not started/over");

        require(level == currentLevel, "Random: invalid level");

        require(
            amount <= getMaxTablesPerTransactionForLevel(level),
            "Random: max tables exceeded"
        );
        require(
            msgValue >= getTablePriceForLevel(level) * amount,
            "Random: insufficient value"
        );

        if (currentLevel != 1) {
            _verifyPrevLevelTables(user, level - 1);
        }
    }

    function _verifyPrevLevelTables(address user, uint256 level) private view {
        require(
            userTablesCount(level, user) >= 1 ||
                alchemestry.queuePoolUserTablesCount(level, user) >= 1,
            "Random: insufficient level tables"
        );

        if (level != 1) {
            _verifyPrevLevelTables(user, level - 1);
        }
    }
}
