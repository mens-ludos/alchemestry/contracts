// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.24;

import {AlchemestryInitializable} from "./abstract/AlchemestryInitializable.sol";
import {IPool} from "./interfaces/IPool.sol";
import {Alchemestry} from "./Alchemestry.sol";
import {GovernorPool} from "./abstract/GovernorPool.sol";

contract ReferralCenter is AlchemestryInitializable {
    address public constant NO_REFERRER_ADDRESS = address(0xdead);

    address public alchemestry;
    address public queuePool;
    address public randomPool;

    struct UserRewardInfo {
        address referrer;
        uint256 totalRewards;
        uint256 claimedRewards;
    }

    event CreateUser(address indexed user, address indexed referrer);

    event AddRewards(address indexed user, uint256 amount);

    event ClaimRewards(address indexed user, uint256 amount);

    mapping(address => UserRewardInfo) public userRewardInfo;

    modifier autorized() {
        require(
            msg.sender == queuePool || msg.sender == randomPool,
            "RC: not authorized"
        );
        _;
    }

    function initialize(
        address payable _alchemestry,
        address _queuePool,
        address _randomPool
    ) external initializer {
        alchemestry = _alchemestry;
        queuePool = _queuePool;
        randomPool = _randomPool;
    }

    function createUserIfNotExists(
        address user,
        address referrer
    ) external autorized returns (bool created) {
        require(user != address(0), "RC: invalid user");
        if (isUserExists(user)) return false;

        if (referrer != address(0)) {
            require(isUserExists(referrer), "RC: r not exists");
        } else {
            referrer = NO_REFERRER_ADDRESS;
        }

        userRewardInfo[user].referrer = referrer;

        emit CreateUser(user, referrer);
    }

    function addRewards(address rewardsFor) external payable autorized {
        require(msg.value > 0, "RC: nothing to add");
        require(isUserExists(rewardsFor), "RC: user not exists");

        userRewardInfo[rewardsFor].totalRewards += msg.value;

        emit AddRewards(rewardsFor, msg.value);
    }

    function claimRewards(
        address payable claimFor
    ) external returns (uint256 claimedAmount) {
        require(isUserExists(claimFor), "RC: user not exists");

        UserRewardInfo memory info = userRewardInfo[claimFor];

        claimedAmount = info.totalRewards - info.claimedRewards;

        require(claimedAmount > 0, "RC: nothing to claim");

        info.claimedRewards += claimedAmount;

        userRewardInfo[claimFor] = info;

        claimFor.transfer(claimedAmount);

        emit ClaimRewards(claimFor, claimedAmount);
    }

    function isUserExists(address user) public view returns (bool) {
        return userRewardInfo[user].referrer != address(0);
    }
}
