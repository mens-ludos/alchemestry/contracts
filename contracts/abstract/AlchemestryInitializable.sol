// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.24;

import {Initializable} from "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

abstract contract AlchemestryInitializable is Initializable {
    constructor() {
        _disableInitializers();
    }
}
