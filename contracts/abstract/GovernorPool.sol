// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.24;

import {OwnableUpgradeable} from "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import {AlchemestryInitializable} from "./AlchemestryInitializable.sol";
import {Alchemestry} from "../Alchemestry.sol";
import {ReferralCenter} from "../ReferralCenter.sol";

abstract contract GovernorPool is OwnableUpgradeable, AlchemestryInitializable {
    Alchemestry public alchemestry;

    uint256 public totalGovernanceFees;
    uint256 public governanceFeesCollected;

    function __GovernorPool_init(
        address _owner,
        address payable _alchemestry
    ) internal {
        __Ownable_init(_owner);

        require(_alchemestry != address(0), "Queue: invalid _alchemestry");
        alchemestry = Alchemestry(_alchemestry);

        _transferOwnership(_owner);
    }

    function claimGovernanceFees(address collectTo) external returns (uint256) {
        require(
            msg.sender == owner() || msg.sender == address(alchemestry),
            "Queue: invalid sender"
        );

        require(collectTo != address(0), "Queue: invalid collectTo");

        uint256 toCollect = totalGovernanceFees - governanceFeesCollected;

        governanceFeesCollected = totalGovernanceFees;

        payable(collectTo).transfer(toCollect);

        return toCollect;
    }

    function addReferralRewards(address user, uint256 referralFee) internal {
        if (referralFee == 0) return;
        ReferralCenter(alchemestry.referralCenter()).addRewards{
            value: referralFee
        }(user);
    }

    function claimReferralRewards(address user) internal {
        ReferralCenter(alchemestry.referralCenter()).claimRewards(
            payable(user)
        );
    }

    function collectGovernanceFee(uint256 governanceFee) internal {
        totalGovernanceFees += governanceFee;
    }
}
