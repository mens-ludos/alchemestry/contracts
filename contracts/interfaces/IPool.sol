// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.24;

interface IPool {
    function getTablePriceForLevel(
        uint256 level
    ) external view returns (uint256);

    function getTableFulFillForLevel(
        uint256 level
    ) external view returns (uint256);

    function getMaxTablesPerTransactionForLevel(
        uint256 level
    ) external view returns (uint256);

    function userTablesCount(
        uint256 _level,
        address _user
    ) external view returns (uint256);

    function calculateFees(
        uint256 level,
        uint256 amount,
        address referral
    )
        external
        view
        returns (
            uint256 globalFee,
            uint256 governanceFee,
            uint256 referralFee,
            uint256 singleTablePriceWithoutFees
        );
}
