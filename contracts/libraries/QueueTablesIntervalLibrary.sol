// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.24;

struct QueueTablesInterval {
    uint256 idFrom;
    uint256 idTo;
    address owner;
    uint256 fullFilledAmount;
    uint256 _next;
    uint256 _prev;
}

library QueueTablesIntervalLibrary {
    function next(
        QueueTablesInterval storage interval
    ) internal view returns (uint256) {
        return interval._next;
    }

    function prev(
        QueueTablesInterval storage interval
    ) internal view returns (uint256) {
        return interval._prev;
    }

    function isDeleted(
        QueueTablesInterval storage interval
    ) internal view returns (bool) {
        return interval.owner == address(0);
    }
}
