// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.24;

library RandomStatusLibrary {
    struct RandomStatus {
        uint256 tablesFilled;
        uint256 lastRequestId;
        bool isLastRequestPending;
        bool isFilled;
    }

    function leftToFill(
        RandomStatus storage status,
        uint256 requiredTablesCount
    ) internal view returns (uint256) {
        return requiredTablesCount - status.tablesFilled;
    }

    function updateStatus(
        RandomStatus storage status,
        uint256 increaseTableFilled,
        uint256 lastRequestId,
        bool isFilled
    ) internal {
        status.tablesFilled += increaseTableFilled;
        status.lastRequestId = lastRequestId;
        status.isFilled = isFilled;
        delete status.isLastRequestPending;
    }
}
