// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.24;

import {QueueTablesInterval} from "./QueueTablesIntervalLibrary.sol";

library TableIntervalLibrary {
    function tables(
        QueueTablesInterval storage interval
    ) internal view returns (uint256) {
        return interval.idTo - interval.idFrom;
    }

    function leftToFullFill(
        QueueTablesInterval storage interval,
        uint256 tableFulfillSize
    ) internal view returns (uint256) {
        return tables(interval) * tableFulfillSize - interval.fullFilledAmount;
    }
}
