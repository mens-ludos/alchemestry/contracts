// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.24;

import {QueueTablesInterval, QueueTablesIntervalLibrary} from "./QueueTablesIntervalLibrary.sol";

library TableIntervalsQueueLibrary {
    uint256 constant NULL = type(uint256).max;

    using QueueTablesIntervalLibrary for QueueTablesInterval;

    struct TableIntervalsQueue {
        uint256 _indexesCounter;
        mapping(uint256 => QueueTablesInterval) _nodes;
        uint256 _count;
        uint256 _head;
        uint256 _tail;
    }

    function length(
        TableIntervalsQueue storage q
    ) internal view returns (uint256) {
        return q._count;
    }

    function head(
        TableIntervalsQueue storage q
    ) internal view returns (uint256) {
        return q._head;
    }

    function tail(
        TableIntervalsQueue storage q
    ) internal view returns (uint256) {
        return q._tail;
    }

    function push(
        TableIntervalsQueue storage q,
        QueueTablesInterval memory data
    ) internal {
        q._count++;
        uint256 newTailId = q._indexesCounter++;

        data._next = NULL;

        // list is empty
        if (q._head == NULL) {
            q._head = newTailId;
            data._prev = NULL;
        } else {
            data._prev = q._tail;
            q._nodes[q._tail]._next = newTailId;
        }

        q._nodes[newTailId] = data;

        q._tail = newTailId;
    }

    function pop(
        TableIntervalsQueue storage q
    ) internal returns (QueueTablesInterval memory r) {
        q._count--;

        require(q._head != NULL, "TIQL: Queue is Empty");

        if (q._head == q._tail) {
            delete q._nodes[q._tail];

            q._head = NULL;
            q._tail = NULL;
        } else {
            uint256 _tailToDel = q._tail;
            r = q._nodes[_tailToDel];

            q._tail = q._nodes[q._tail]._prev;
            q._nodes[q._tail]._next = NULL;

            delete q._nodes[_tailToDel];
        }
    }

    function viewLast(
        TableIntervalsQueue storage q
    ) internal view returns (QueueTablesInterval memory) {
        return q._nodes[q._tail];
    }

    function viewFirst(
        TableIntervalsQueue storage q
    ) internal view returns (QueueTablesInterval storage) {
        return q._nodes[q._head];
    }

    function updateLast(
        TableIntervalsQueue storage q,
        QueueTablesInterval memory newValue
    ) internal {
        q._nodes[q._tail] = newValue;
    }

    function viewByNodeId(
        TableIntervalsQueue storage q,
        uint256 id
    ) internal view returns (QueueTablesInterval storage) {
        return q._nodes[id];
    }

    function deleteByNodeId(
        TableIntervalsQueue storage q,
        uint256 id
    ) internal {
        require(q._head != NULL, "TIQL: Queue is Empty");

        if (id == q._tail) {
            pop(q);
            return;
        }

        q._count--;

        QueueTablesInterval memory i = q._nodes[id];

        if (id == q._head) {
            q._head = i._next;
        }

        if (i._prev != NULL) {
            q._nodes[i._prev]._next = i._next;
        }

        if (i._next != NULL) {
            q._nodes[i._next]._prev = i._prev;
        }

        delete q._nodes[id];
    }
}
