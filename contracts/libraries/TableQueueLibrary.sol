// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.24;

import {QueueTablesInterval, QueueTablesIntervalLibrary} from "./QueueTablesIntervalLibrary.sol";
import {TableIntervalsQueueLibrary} from "./TableIntervalsQueueLibrary.sol";
import {TableIntervalLibrary} from "./TableIntervalLibrary.sol";

library TableQueueLibrary {
    using TableIntervalsQueueLibrary for TableIntervalsQueueLibrary.TableIntervalsQueue;
    using TableIntervalLibrary for QueueTablesInterval;
    using QueueTablesIntervalLibrary for QueueTablesInterval;

    struct QueueUserInfo {
        address referrer;
        uint256 tablesCount;
        uint256 referralRewards;
        uint256 totalFilledAmount;
        uint256 totalClaimedAmount;
    }

    struct TableQueue {
        TableIntervalsQueueLibrary.TableIntervalsQueue intervalsQueue;
        mapping(address => QueueUserInfo) userInfos;
    }

    event AddTables(
        address indexed caller,
        uint256 tablesCount,
        uint256 tablePrice
    );

    event FulfillTable(
        address indexed caller,
        address indexed tablesOwner,
        uint256 tableFulfillAmount,
        uint256 fulfilled
    );

    event ClaimRewards(address indexed caller, uint256 claimedAmount);

    function addMult(
        TableQueue storage queue,
        address user,
        uint256 tablesCount,
        uint256 tablePrice,
        uint256 tableFulfillAmount
    ) internal returns (uint256) {
        require(user != address(0), "TQL: user == 0");
        // TODO: total gas cost optimization
        QueueUserInfo storage userInfo = queue.userInfos[user];

        userInfo.tablesCount += tablesCount;

        QueueTablesInterval memory lastInterval;

        uint256 queueLength = queue.intervalsQueue.length();

        if (queueLength != 0) {
            lastInterval = queue.intervalsQueue.viewLast();
        }

        if (lastInterval.owner == user) {
            lastInterval.idTo += tablesCount;

            queue.intervalsQueue.updateLast(lastInterval);
        } else {
            uint256 idFrom = 0;
            uint256 idTo;

            if (lastInterval.owner != address(0)) {
                idFrom = lastInterval.idFrom + 1;
            }

            idTo = idFrom + tablesCount;

            // if (queueLength != 0) {
            //     queue.intervalsQueue.updateLast(lastInterval);
            // }

            queue.intervalsQueue.push(
                QueueTablesInterval({
                    idFrom: idFrom,
                    idTo: idTo,
                    owner: user,
                    fullFilledAmount: 0,
                    _next: 0,
                    _prev: 0
                })
            );
        }

        emit AddTables(user, tablesCount, tablePrice);

        return
            fullFill(queue, user, tablesCount, tablePrice, tableFulfillAmount);
    }

    function fullFill(
        TableQueue storage queue,
        address sender,
        uint256 tablesCount,
        uint256 tablePrice,
        uint256 tableFulfillAmount
    ) internal returns (uint256 notFilledAmount) {
        uint256 fullFillLeft = tablesCount * tablePrice;

        if (queue.intervalsQueue.length() == 0) {
            return fullFillLeft;
        }

        QueueTablesInterval storage firstInterval;

        uint256 next = queue.intervalsQueue.head();
        uint256 tail = queue.intervalsQueue.tail();

        // nothing to fill or only our tables are left in the queue
        while (fullFillLeft != 0 && next != TableIntervalsQueueLibrary.NULL) {
            firstInterval = queue.intervalsQueue.viewByNodeId(next);

            if (firstInterval.owner == sender) {
                next = firstInterval.next();
                continue;
            }

            uint256 intervalFullFillLeft = firstInterval.leftToFullFill(
                tableFulfillAmount
            );

            if (fullFillLeft >= intervalFullFillLeft) {
                queue
                    .userInfos[firstInterval.owner]
                    .totalFilledAmount += intervalFullFillLeft;
                fullFillLeft -= intervalFullFillLeft;

                uint256 newNext = firstInterval.next();
                queue.intervalsQueue.deleteByNodeId(next);
                next = newNext;

                emit FulfillTable(
                    sender,
                    firstInterval.owner,
                    tableFulfillAmount,
                    intervalFullFillLeft
                );
            } else {
                queue
                    .userInfos[firstInterval.owner]
                    .totalFilledAmount += fullFillLeft;
                firstInterval.fullFilledAmount += fullFillLeft;

                emit FulfillTable(
                    sender,
                    firstInterval.owner,
                    fullFillLeft,
                    intervalFullFillLeft
                );

                fullFillLeft = 0;

                break;
            }
        }

        return (fullFillLeft);
    }

    function claim(
        TableQueue storage queue,
        address user
    ) internal returns (uint256 claimed) {
        claimed =
            queue.userInfos[user].totalFilledAmount -
            queue.userInfos[user].totalClaimedAmount;

        require(claimed > 0, "TQL: nothing to claim");

        queue.userInfos[user].totalClaimedAmount += claimed;

        emit ClaimRewards(user, claimed);
    }
}
