// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.24;

import {RandomStatusLibrary} from "./RandomStatusLibrary.sol";

library TableRandomPoolLibrary {
    using RandomStatusLibrary for RandomStatusLibrary.RandomStatus;

    struct Table {
        address owner;
    }

    struct RandomPoolInfo {
        Table[] tables;
        uint256 poolSize;
        uint256 claimed;
        bool isPoolManuallyFilled;
        RandomStatusLibrary.RandomStatus randomStatus;
    }

    struct RandomPoolUserInfo {
        uint256 tablesCount;
        uint256 wonTablesCount;
        uint256 totalClaimedAmount;
        // uint256 referralRewards;
    }

    struct RandomPoolLevel {
        RandomPoolInfo pool;
        mapping(address => RandomPoolUserInfo) userInfos;
    }

    function addTables(
        RandomPoolLevel storage pool,
        address user,
        uint256 tablesCount,
        uint256 tablePrice,
        uint256 /*tableRewardAmount*/
    ) internal {
        pool.userInfos[user].tablesCount += tablesCount;

        // if (referral != address(0) && referralFees != 0) {
        //     pool.userInfos[referral].referralRewards += referralFees;
        // }

        pool.pool.poolSize += tablePrice * tablesCount;

        Table memory table = Table({owner: user});

        // push it to the array n times to assign index to each entity
        for (uint256 i; i < tablesCount; i++) {
            pool.pool.tables.push(table);
        }
    }

    /// @notice manually fills random pool, to fill all tables of all participated users
    /// @param pool - pool storage reference
    /// @param tableRewardAmount - reward per table
    /// @param valueProvided - msg.value provided to transaction
    function manuallyFillPool(
        RandomPoolLevel storage pool,
        uint256 tableRewardAmount,
        uint256 valueProvided
    ) internal returns (uint256) {
        require(!pool.pool.isPoolManuallyFilled, "TRPL: already filled");

        require(pool.pool.randomStatus.isFilled, "TRPL: random not filled");

        uint256 totalRequiredAmount = tableRewardAmount *
            pool.pool.tables.length -
            pool.pool.claimed;

        require(
            valueProvided >= totalRequiredAmount,
            "TRPL: not enough amount"
        );

        pool.pool.poolSize += totalRequiredAmount;
        pool.pool.isPoolManuallyFilled = true;

        pool.pool = pool.pool;

        return valueProvided - totalRequiredAmount;
    }

    function claimRewards(
        RandomPoolLevel storage pool,
        address user,
        uint256 tableRewardAmount,
        bool /*claimReferral*/
    ) internal returns (uint256) {
        RandomPoolUserInfo memory info = pool.userInfos[user];

        require(pool.pool.randomStatus.isFilled, "TRPL: random not filled");

        if (pool.pool.isPoolManuallyFilled) {
            info.wonTablesCount = info.tablesCount;
        }

        uint256 totalClaimableRewards = info.wonTablesCount *
            tableRewardAmount -
            info.totalClaimedAmount;

        info.totalClaimedAmount += totalClaimableRewards;
        pool.pool.claimed += totalClaimableRewards;

        // if (claimReferral) {
        //     totalClaimableRewards += info.referralRewards;
        //     info.referralRewards = 0;
        // }

        require(totalClaimableRewards > 0, "TRPL: nothing to claim");

        pool.userInfos[user] = info;

        return totalClaimableRewards;
    }

    function fillRandom(
        RandomPoolLevel storage pool,
        uint256[] memory wonIds,
        uint256 totalWinners,
        uint256 requestId
    ) internal returns (uint256) {
        require(!pool.pool.randomStatus.isFilled, "TRPL: already filled");

        require(
            !pool.pool.randomStatus.isLastRequestPending,
            "TRPL: last requets not filled"
        );

        uint256 leftToFill = pool.pool.randomStatus.leftToFill(totalWinners);

        if (leftToFill == 0) {
            pool.pool.randomStatus.isFilled = true;
            return 0;
        }

        require(leftToFill >= wonIds.length, "TRPL: exceeded left to fill");

        pool.pool.randomStatus.updateStatus(
            wonIds.length,
            requestId,
            leftToFill == wonIds.length
        );

        Table memory table;

        for (uint256 i = 0; i < wonIds.length; i++) {
            uint256 wonTableIndex = wonIds[i] % pool.pool.tables.length;

            // get table
            table = pool.pool.tables[wonTableIndex];

            // update user`s won tables counter
            pool.userInfos[table.owner].wonTablesCount += 1;

            // remove winner table from tables list
            pool.pool.tables[wonTableIndex] = pool.pool.tables[
                pool.pool.tables.length - 1
            ];

            pool.pool.tables.pop();
        }

        return leftToFill - wonIds.length;
    }

    function calculateTotalWinners(
        RandomPoolLevel storage pool,
        uint256 tableRewardAmount
    ) internal view returns (uint256) {
        return pool.pool.poolSize / tableRewardAmount;
    }
}
