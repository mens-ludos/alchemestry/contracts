// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.24;

import {Alchemestry} from "../Alchemestry.sol";

contract AlchemestryMock is Alchemestry {
    uint256 _currentLevel;
    bool _ended;

    constructor(
        address _queuePoolMaster,
        address _randomPoolMaster,
        address _referralCenterMaster,
        address _vrfConsumer,
        uint256 _poolStart,
        uint256 _poolLevelDuration,
        uint256 _poolLevels
    )
        Alchemestry(
            _queuePoolMaster,
            _randomPoolMaster,
            _referralCenterMaster,
            _vrfConsumer,
            _poolStart,
            _poolLevelDuration,
            _poolLevels
        )
    {}

    function setNextLevel() external {
        if (!_ended && _currentLevel < poolLevels) {
            _currentLevel++;
        } else {
            delete _currentLevel;
            _ended = true;
        }
    }

    function getCurrentActiveLevel() external view override returns (uint256) {
        return _currentLevel;
    }
}
