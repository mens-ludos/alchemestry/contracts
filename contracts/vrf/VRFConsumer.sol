// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.24;

import {VRFCoordinatorV2Interface} from "@chainlink/contracts/src/v0.8/vrf/interfaces/VRFCoordinatorV2Interface.sol";
import {VRFConsumerBaseV2} from "@chainlink/contracts/src/v0.8/vrf/VRFConsumerBaseV2.sol";
import {Ownable} from "@openzeppelin/contracts/access/Ownable.sol";

import {RandomPool} from "../RandomPool.sol";

contract VRFConsumer is VRFConsumerBaseV2, Ownable(msg.sender) {
    struct RequestStatus {
        bool fulfilled; // whether the request has been successfully fulfilled
        bool exists; // whether a requestId exists
        uint256 randomWordsLength;
        uint256 forLevel;
    }

    // TODO: adjust it after testing
    uint32 public constant CALLBACK_GAS_LIMIT_PER_WORD = 100_000;

    uint16 public constant REQUEST_CONFIRMATIONS = 3;

    mapping(uint256 => RequestStatus) public vrfRequests;

    address public randomPool;

    uint256 public lastRequestId;

    VRFCoordinatorV2Interface private immutable _coordinator;

    uint64 private immutable _subscriptionId;

    bytes32 private constant KEY_HASH =
        0x79d3d8832d904592c0bf9818b621522c988bb8b0c05cdc3b15aea1b6e8db0c15;

    event RequestSent(uint256 requestId, uint32 numWords);
    event RequestFulfilled(uint256 requestId);

    modifier onlyFromPool() {
        address pool = randomPool;

        require(pool != address(0), "VRF: pool is not initialized");
        require(msg.sender == pool, "VRF: sender not pool");

        _;
    }

    constructor(
        address vrfCoordinator,
        uint64 subscriptionId
    ) VRFConsumerBaseV2(vrfCoordinator) {
        _coordinator = VRFCoordinatorV2Interface(vrfCoordinator);
        _subscriptionId = subscriptionId;
    }

    function initialize(address _pool) external onlyOwner {
        require(randomPool == address(0), "VRF: pool is already set");
        require(_pool != address(0), "VRF: 0 _pool");
        require(
            RandomPool(_pool).vrfConsumer() == address(this),
            "VRF: invalid _pool"
        );

        randomPool = _pool;
    }

    // Assumes the subscription is funded sufficiently.
    function requestRandomWords(
        uint32 numWords,
        uint256 forLevel
    ) external onlyFromPool returns (uint256 requestId) {
        // Will revert if subscription is not set and funded.
        requestId = _coordinator.requestRandomWords(
            KEY_HASH,
            _subscriptionId,
            REQUEST_CONFIRMATIONS,
            numWords * CALLBACK_GAS_LIMIT_PER_WORD,
            numWords
        );

        vrfRequests[requestId] = RequestStatus({
            randomWordsLength: numWords,
            exists: true,
            fulfilled: false,
            forLevel: forLevel
        });

        lastRequestId = requestId;

        emit RequestSent(requestId, numWords);
        return requestId;
    }

    function fulfillRandomWords(
        uint256 _requestId,
        uint256[] memory _randomWords
    ) internal override {
        RequestStatus memory status = vrfRequests[_requestId];
        require(status.exists, "VRF: request not found");
        require(!status.fulfilled, "VRF: already filled");

        status.fulfilled = true;

        RandomPool(randomPool).fillRandom(
            _randomWords,
            status.forLevel,
            _requestId
        );

        vrfRequests[_requestId] = status;
        emit RequestFulfilled(_requestId);
    }

    function getRequestStatus(
        uint256 _requestId
    ) external view returns (RequestStatus memory) {
        require(vrfRequests[_requestId].exists, "request not found");
        return vrfRequests[_requestId];
    }
}
