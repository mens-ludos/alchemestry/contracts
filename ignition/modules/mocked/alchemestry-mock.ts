import { buildModule } from '@nomicfoundation/hardhat-ignition/modules';
import { artifacts } from 'hardhat';

import VRFConsumerMockModule from '@/ignition/modules/mocked/vrf-consumer-mock';
import QueuePoolMasterModule from '@/ignition/modules/queue-pool-master';
import RandomPoolMasterModule from '@/ignition/modules/random-pool-master';
import ReferralCenterMasterModule from '@/ignition/modules/referral-center-master';

const NAME = 'AlchemestryMock';

const AlchemestryMockModule = buildModule(NAME, (m) => {
  const { queuePoolMaster } = m.useModule(QueuePoolMasterModule);
  const { randomPoolMaster } = m.useModule(RandomPoolMasterModule);
  const { referralCenterMaster } = m.useModule(ReferralCenterMasterModule);
  const { vrfConsumer } = m.useModule(VRFConsumerMockModule);

  const startTimestamp = m.getParameter<number>('game.startTimestamp');
  const levelDuration = m.getParameter<number>('game.levelDuration');
  const levelsAmount = m.getParameter<number>('game.levelsAmount');

  const alchemestryMock = m.contract(NAME, artifacts.readArtifactSync(NAME), [
    queuePoolMaster,
    randomPoolMaster,
    referralCenterMaster,
    vrfConsumer,
    startTimestamp,
    levelDuration,
    levelsAmount,
  ]);

  const randomPool = m.staticCall(alchemestryMock, 'randomPool');
  m.call(vrfConsumer, 'initialize', [randomPool]);

  return { alchemestryMock };
});

export default AlchemestryMockModule;
