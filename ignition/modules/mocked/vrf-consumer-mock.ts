import { buildModule } from '@nomicfoundation/hardhat-ignition/modules';
import { artifacts } from 'hardhat';

import VRFCoordinatorMockModule from '@/ignition/modules/mocked/vrf-coordinator-mock';

const NAME = 'VRFConsumer';

const VRFConsumerMockModule = buildModule(`${NAME}Mock`, (m) => {
  const subscriptionId = m.getParameter<number>('subscriptionId', 0);
  const { vrfCoordinatorMock } = m.useModule(VRFCoordinatorMockModule);
  const vrfConsumer = m.contract(NAME, artifacts.readArtifactSync(NAME), [
    vrfCoordinatorMock,
    subscriptionId,
  ]);

  return { vrfConsumer };
});

export default VRFConsumerMockModule;
