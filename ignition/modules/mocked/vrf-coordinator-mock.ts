import { buildModule } from '@nomicfoundation/hardhat-ignition/modules';
import { artifacts } from 'hardhat';

const NAME = 'VRFCoordinatorMock';

const VRFCoordinatorMockModule = buildModule(NAME, (m) => {
  const vrfCoordinatorMock = m.contract(NAME, artifacts.readArtifactSync(NAME));

  return { vrfCoordinatorMock };
});

export default VRFCoordinatorMockModule;
