import { buildModule } from '@nomicfoundation/hardhat-ignition/modules';
import { artifacts } from 'hardhat';

const NAME = 'QueuePool';

const QueuePoolMasterModule = buildModule(NAME, (m) => {
  const queuePoolMaster = m.contract(NAME, artifacts.readArtifactSync(NAME));

  return { queuePoolMaster };
});

export default QueuePoolMasterModule;
