import { buildModule } from '@nomicfoundation/hardhat-ignition/modules';
import { artifacts } from 'hardhat';

const NAME = 'RandomPool';

const RandomPoolMasterModule = buildModule(NAME, (m) => {
  const randomPoolMaster = m.contract(NAME, artifacts.readArtifactSync(NAME));

  return { randomPoolMaster };
});

export default RandomPoolMasterModule;
