import { buildModule } from '@nomicfoundation/hardhat-ignition/modules';
import { artifacts } from 'hardhat';

const NAME = 'ReferralCenter';

const ReferralCenterMasterModule = buildModule(NAME, (m) => {
  const referralCenterMaster = m.contract(
    NAME,
    artifacts.readArtifactSync(NAME),
  );

  return { referralCenterMaster };
});

export default ReferralCenterMasterModule;
