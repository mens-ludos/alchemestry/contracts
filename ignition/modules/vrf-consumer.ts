import { buildModule } from '@nomicfoundation/hardhat-ignition/modules';
import { artifacts } from 'hardhat';
import { type Address } from 'viem';

const NAME = 'VRFConsumer';

const VRFConsumerModule = buildModule(NAME, (m) => {
  const coordinator = m.getParameter<Address>('coordinator');
  const subscriptionId = m.getParameter<number>('subscriptionId');

  const vrfConsumer = m.contract(NAME, artifacts.readArtifactSync(NAME), [
    coordinator,
    subscriptionId,
  ]);

  return { vrfConsumer };
});

export default VRFConsumerModule;
