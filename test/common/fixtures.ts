import {
  time,
  setNextBlockBaseFeePerGas,
} from '@nomicfoundation/hardhat-toolbox-viem/network-helpers';
import { viem } from 'hardhat';

export const deployWithMockedConsumer = async () => {
  const [owner, ...regularAccounts] = await viem.getWalletClients();

  await setNextBlockBaseFeePerGas(0);

  const queuePoolMaster = await viem.deployContract('QueuePool', [], {
    client: { wallet: owner },
  });
  const randomPoolMaster = await viem.deployContract('RandomPool', [], {
    client: { wallet: owner },
  });
  const referralCenterMaster = await viem.deployContract('ReferralCenter', [], {
    client: { wallet: owner },
  });

  const vrfCoordinator = await viem.deployContract('VRFCoordinatorMock', [], {
    client: { wallet: owner },
  });

  const vrfConsumer = await viem.deployContract(
    'VRFConsumer',
    [vrfCoordinator.address, 0n],
    {
      client: { wallet: owner },
    },
  );

  const poolStart = BigInt((await time.latest()) + time.duration.days(1));
  const poolDuration = BigInt(time.duration.days(1));
  const poolLevels = 3n;

  const alchemestry = await viem.deployContract(
    'Alchemestry',
    [
      queuePoolMaster.address,
      randomPoolMaster.address,
      referralCenterMaster.address,
      vrfConsumer.address,
      poolStart,
      poolDuration,
      poolLevels,
    ],
    {
      client: { wallet: owner },
    },
  );

  await vrfConsumer.write.initialize([await alchemestry.read.randomPool()]);

  const queuePool = await viem.getContractAt(
    'QueuePool',
    await alchemestry.read.queuePool(),
    { client: { wallet: owner } },
  );

  const randomPool = await viem.getContractAt(
    'RandomPool',
    await alchemestry.read.randomPool(),
    { client: { wallet: owner } },
  );

  const referralCenter = await viem.getContractAt(
    'ReferralCenter',
    await alchemestry.read.referralCenter(),
    { client: { wallet: owner } },
  );

  return {
    regularAccounts,
    owner,
    queuePoolMaster,
    randomPoolMaster,
    referralCenterMaster,
    vrfCoordinator,
    vrfConsumer,
    alchemestry,
    queuePool,
    randomPool,
    referralCenter,
    alchemestryParams: {
      poolStart,
      poolDuration,
      poolLevels,
    },
  };
};
