import { time } from '@nomicfoundation/hardhat-toolbox-viem/network-helpers';
import { assert } from 'chai';

import { type Alchemestry$Type } from '@/artifacts/contracts/Alchemestry.sol/Alchemestry';
import { type Contract } from '@/config/types';

export const waitForLevel = async ({
  alchemestry,
  level = 1n,
}: {
  alchemestry: Contract<Alchemestry$Type>;
  level?: bigint;
}) => {
  assert(level > 0n, 'TEST waitForLevel: Invalid level provided');

  const levelStart = await alchemestry.read.getLevelStartTimestamp([level]);

  const currTime = await time.latest();

  if (levelStart > currTime) {
    await time.setNextBlockTimestamp(levelStart);
  }
};
