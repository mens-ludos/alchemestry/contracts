import {
  type GetContractReturnType,
  type WalletClient,
} from '@nomicfoundation/hardhat-viem/types';
import { assert, expect } from 'chai';
import hre from 'hardhat';
import { type Address, zeroAddress } from 'viem';

import { type QueuePool$Type } from '@/artifacts/contracts/QueuePool.sol/QueuePool';
import { type Contract } from '@/config/types';

export const buyTablesMultTest = async ({
  queuePool,
  buyInfos,
}: {
  queuePool: Contract<QueuePool$Type>;
  buyInfos: Array<{
    buyer?: WalletClient;
    level?: bigint;
    amount?: bigint;
    referral?: Address;
    revertMsg?: string;
    msgValue?: bigint;
  }>;
}) => {
  for (const {
    buyer,
    level,
    amount,
    referral,
    msgValue,
    revertMsg,
  } of buyInfos) {
    await buyTables({
      queuePool,
      buyer,
      level,
      amount,
      referral,
      msgValue,
      revertMsg,
    });
  }
};

export const buyTables = async ({
  buyer,
  queuePool,
  level = 1n,
  amount = 1n,
  referral = zeroAddress,
  msgValue,
  revertMsg,
}: {
  buyer?: WalletClient;
  queuePool: GetContractReturnType<QueuePool$Type['abi']>;
  level?: bigint;
  amount?: bigint;
  referral?: Address;
  revertMsg?: string;
  msgValue?: bigint;
}) => {
  const tablePrice = await queuePool.read.getTablePriceForLevel([level]);

  console.log('\n=== Buy start ===\n');
  if (!buyer) {
    [buyer] = await hre.viem.getWalletClients();
  }

  if (!msgValue) {
    msgValue = tablePrice * amount;
  }

  if (revertMsg) {
    await expect(
      // queuePool.write.buyTables(level, amount, referral, { value: msgValue }),
      queuePool.write.buyTables([level, amount, referral], {
        value: msgValue,
        account: buyer.account,
      }),
    ).rejectedWith(revertMsg);
    return;
  }

  if (
    referral !== zeroAddress &&
    (await queuePool.read.userTablesCount([level, referral])) === 0n
  ) {
    referral = zeroAddress;
  }

  const sender = await buyer.account.address;

  const infoBefore = await queuePool.read.userLevelInfo([level, sender]);
  const refInfoBefore = await queuePool.read.userLevelInfo([level, referral]);
  const govFeesBefore = await queuePool.read.totalGovernanceFees();
  const fees = await queuePool.read.calculateFees([level, amount, referral]);

  await expect(
    queuePool.write.buyTables([level, amount, referral], {
      value: msgValue,
      account: buyer.account,
    }),
  ).not.rejected;

  const infoAfter = await queuePool.read.userLevelInfo([level, sender]);
  const refInfoAfter = await queuePool.read.userLevelInfo([level, referral]);
  const govFeesAfter = await queuePool.read.totalGovernanceFees();

  expect(infoAfter.tablesCount).eq(infoBefore.tablesCount + amount);
  expect(infoAfter.totalClaimedAmount).eq(infoBefore.totalClaimedAmount);
  expect(infoAfter.totalFilledAmount).eq(infoBefore.totalFilledAmount);

  assert(
    refInfoAfter.totalFilledAmount >= refInfoBefore.totalFilledAmount + fees[2],
  );
  assert(govFeesAfter >= govFeesBefore + fees[1]);

  console.log('\n=== Buy end ===\n');
};

export const claimTableRewards = async ({
  owner,
  expectedFilledTables,
  queuePool,
  level = 1n,
}: {
  owner: Address;
  expectedFilledTables: bigint;
  queuePool: GetContractReturnType<QueuePool$Type['abi']>;
  level?: bigint;
}) => {
  await expectFilledTables({
    owner,
    queuePool,
    level,
    expectedCount: expectedFilledTables,
  });

  const infoBefore = await queuePool.read.userLevelInfo([level, owner]);

  const tableFullfilAmount = await queuePool.read.getTableFulFillForLevel([
    level,
  ]);
  const expectedClaimed = tableFullfilAmount * expectedFilledTables;

  const publicClient = await hre.viem.getPublicClient();
  const balanceBefore = await publicClient.getBalance({ address: owner });

  if (expectedFilledTables === 0n) {
    await expect(
      queuePool.write.claimTableRewards([level, owner]),
    ).rejectedWith('TQL: nothing to claim');
  } else {
    await expect(queuePool.write.claimTableRewards([level, owner])).not
      .rejected;
  }

  const balanceAfter = await publicClient.getBalance({ address: owner });

  const infoAfter = await queuePool.read.userLevelInfo([level, owner]);

  expect(infoAfter.totalClaimedAmount).eq(
    infoBefore.totalClaimedAmount + expectedClaimed,
  );
  expect(balanceAfter).eq(balanceBefore + expectedClaimed);
};

export const expectFilledTables = async ({
  owner,
  expectedCount,
  queuePool,
  level = 1n,
}: {
  owner: Address;
  expectedCount: bigint;
  queuePool: GetContractReturnType<QueuePool$Type['abi']>;
  level?: bigint;
}) => {
  const userInfo = await queuePool.read.userLevelInfo([level, owner]);

  const tableFullfilAmount = await queuePool.read.getTableFulFillForLevel([
    level,
  ]);

  const fulfilledTables =
    userInfo.totalFilledAmount === 0n
      ? 0n
      : userInfo.totalFilledAmount / tableFullfilAmount;

  expect(fulfilledTables).eq(expectedCount);
};
