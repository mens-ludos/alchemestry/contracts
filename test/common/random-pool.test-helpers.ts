import { type WalletClient } from '@nomicfoundation/hardhat-viem/types';
import { assert, expect } from 'chai';
import hre from 'hardhat';
import { type Address, zeroAddress } from 'viem';

import { type RandomPool$Type } from '@/artifacts/contracts/RandomPool.sol/RandomPool';
import { type ReferralCenter$Type } from '@/artifacts/contracts/ReferralCenter.sol/ReferralCenter';
import { type Contract } from '@/config/types';

export const buyTablesMultTest = async ({
  randomPool,
  referralCenter,
  buyInfos,
}: {
  randomPool: Contract<RandomPool$Type>;
  referralCenter: Contract<ReferralCenter$Type>;
  buyInfos: Array<{
    buyer?: WalletClient;
    level?: bigint;
    amount?: bigint;
    referral?: Address;
    revertMsg?: string;
    msgValue?: bigint;
  }>;
}) => {
  for (const {
    buyer,
    level,
    amount,
    referral,
    msgValue,
    revertMsg,
  } of buyInfos) {
    await buyTables({
      randomPool,
      buyer,
      referralCenter,
      level,
      amount,
      referral,
      msgValue,
      revertMsg,
    });
  }
};

export const buyTables = async ({
  buyer,
  randomPool,
  referralCenter,
  level = 1n,
  amount = 1n,
  referral = zeroAddress,
  msgValue,
  revertMsg,
}: {
  buyer?: WalletClient;
  referralCenter: Contract<ReferralCenter$Type>;
  randomPool: Contract<RandomPool$Type>;
  level?: bigint;
  amount?: bigint;
  referral?: Address;
  revertMsg?: string;
  msgValue?: bigint;
}) => {
  const tablePrice = await randomPool.read.getTablePriceForLevel([level]);

  console.log('\n=== Buy start ===\n');
  if (!buyer) {
    [buyer] = await hre.viem.getWalletClients();
  }

  if (!msgValue) {
    msgValue = tablePrice * amount;
  }

  if (revertMsg) {
    await expect(
      randomPool.write.buyTables([level, amount, referral], {
        value: msgValue,
        account: buyer.account,
      }),
    ).rejectedWith(revertMsg);
    return;
  }

  if (
    referral !== zeroAddress &&
    (await randomPool.read.userTablesCount([level, referral])) === 0n
  ) {
    referral = zeroAddress;
  }

  const sender = await buyer.account.address;

  const infoBefore = await randomPool.read.userLevelInfo([level, sender]);
  const refInfoBefore = await referralCenter.read.userRewardInfo([referral]);
  const govFeesBefore = await randomPool.read.totalGovernanceFees();
  const fees = await randomPool.read.calculateFees([level, amount, referral]);
  const poolInfoBefore = await randomPool.read.poolInfo([level]);

  await expect(
    randomPool.write.buyTables([level, amount, referral], {
      value: msgValue,
      account: buyer.account,
    }),
  ).not.rejected;

  const infoAfter = await randomPool.read.userLevelInfo([level, sender]);
  const refInfoAfter = await referralCenter.read.userRewardInfo([referral]);
  const govFeesAfter = await randomPool.read.totalGovernanceFees();
  const poolInfoAfter = await randomPool.read.poolInfo([level]);

  expect(infoAfter.tablesCount).eq(infoBefore.tablesCount + amount);
  expect(infoAfter.totalClaimedAmount).eq(infoBefore.totalClaimedAmount);
  expect(refInfoAfter[1]).eq(refInfoBefore[1] + fees[2]);

  expect(infoAfter.wonTablesCount).eq(0n);
  expect(infoBefore.wonTablesCount).eq(0n);

  assert(govFeesAfter >= govFeesBefore + fees[1]);

  expect(poolInfoAfter[0]).eq(poolInfoBefore[0] + amount);
  expect(poolInfoAfter[1]).eq(poolInfoBefore[1] + fees[3] * amount);
  expect(poolInfoAfter[2]).eq(poolInfoBefore[2]);
  expect(poolInfoAfter[3]).eq(poolInfoBefore[3]).eq(false);

  console.log('\n=== Buy end ===\n');
};

export const claimTableRewards = async ({
  owner,
  expectedFilledTables,
  randomPool,
  level = 1n,
}: {
  owner: Address;
  expectedFilledTables: bigint;
  randomPool: Contract<RandomPool$Type>;
  level?: bigint;
}) => {
  await expectFilledTables({
    owner,
    randomPool,
    level,
    expectedCount: expectedFilledTables,
  });

  const infoBefore = await randomPool.read.userLevelInfo([level, owner]);

  const tableFullfilAmount = await randomPool.read.getTableFulFillForLevel([
    level,
  ]);
  const expectedClaimed = tableFullfilAmount * expectedFilledTables;

  const publicClient = await hre.viem.getPublicClient();
  const balanceBefore = await publicClient.getBalance({ address: owner });

  if (expectedFilledTables === 0n) {
    await expect(
      randomPool.write.claimTableRewards([level, owner]),
    ).rejectedWith('TQL: nothing to claim');
  } else {
    await expect(randomPool.write.claimTableRewards([level, owner])).not
      .rejected;
  }

  const balanceAfter = await publicClient.getBalance({ address: owner });

  const infoAfter = await randomPool.read.userLevelInfo([level, owner]);

  expect(infoAfter.totalClaimedAmount).eq(
    infoBefore.totalClaimedAmount + expectedClaimed,
  );
  expect(balanceAfter).eq(balanceBefore + expectedClaimed);
};

export const expectFilledTables = async ({
  owner,
  expectedCount,
  randomPool,
  level = 1n,
}: {
  owner: Address;
  expectedCount: bigint;
  randomPool: Contract<RandomPool$Type>;
  level?: bigint;
}) => {
  const userInfo = await randomPool.read.userLevelInfo([level, owner]);

  const fulfilledTables = userInfo.wonTablesCount;

  expect(fulfilledTables).eq(expectedCount);
};
