import {
  impersonateAccount,
  setBalance,
  stopImpersonatingAccount,
} from '@nomicfoundation/hardhat-toolbox-viem/network-helpers';
import { expect } from 'chai';
import { viem } from 'hardhat';
import {
  type Address,
  parseEventLogs,
  parseUnits,
  zeroAddress,
  getAddress,
} from 'viem';

import { type QueuePool$Type } from '@/artifacts/contracts/QueuePool.sol/QueuePool';
import { type RandomPool$Type } from '@/artifacts/contracts/RandomPool.sol/RandomPool';
import { type ReferralCenter$Type } from '@/artifacts/contracts/ReferralCenter.sol/ReferralCenter';
import { type Contract } from '@/config/types';
import {
  type AccountOrAddress,
  DEAD_ADDRESS,
  addressOf,
} from '@/test/common/utils';

export const createUserIfNotExists = async ({
  referralCenter,
  caller,
  user,
  revertMsg,
  referrer = zeroAddress,
}: {
  referralCenter: Contract<ReferralCenter$Type>;
  user: Address;
  caller?: AccountOrAddress<QueuePool$Type>;
  referrer?: Address;
  revertMsg?: string;
}) => {
  if (!caller) {
    [caller] = await viem.getWalletClients();
  } else {
    caller = await viem.getWalletClient(addressOf(caller));
  }

  const callParams = [user, referrer] as const;

  if (revertMsg) {
    await expect(
      referralCenter.write.createUserIfNotExists([...callParams], {
        account: caller.account,
      }),
    ).rejectedWith(revertMsg);
    return;
  }

  const isAlreadyRegistered = await referralCenter.read.isUserExists([user]);

  if (!isAlreadyRegistered) {
    if (referrer === zeroAddress) {
      referrer = DEAD_ADDRESS;
    }
    const hash = await referralCenter.write.createUserIfNotExists(
      [...callParams],
      {
        account: caller.account,
      },
    );
    const publicClient = await viem.getPublicClient();
    const receipt = await publicClient.waitForTransactionReceipt({ hash });
    const events = parseEventLogs({
      abi: referralCenter.abi,
      logs: receipt.logs,
    });
    const createUserEvent = events.find(
      (event) =>
        event.eventName === 'CreateUser' &&
        event.args.user === getAddress(user) &&
        event.args.referrer === getAddress(referrer),
    );
    expect(createUserEvent).not.eq(undefined);
  } else {
    await expect(
      referralCenter.write.createUserIfNotExists([...callParams], {
        account: caller.account,
      }),
    ).not.rejected;
    referrer = (await referralCenter.read.userRewardInfo([user]))[0];
  }

  const userInfo = await referralCenter.read.userRewardInfo([user]);

  expect(userInfo[0]).eq(getAddress(referrer));
  expect(userInfo[1]).eq(0n);
  expect(userInfo[2]).eq(0n);
};

export const addRewards = async ({
  referralCenter,
  caller,
  rewardsFor,
  revertMsg,
  msgValueN = 0n,
}: {
  referralCenter: Contract<ReferralCenter$Type>;
  rewardsFor: Address;
  caller?: AccountOrAddress<QueuePool$Type>;
  msgValueN?: bigint;
  revertMsg?: string;
}) => {
  rewardsFor = addressOf(rewardsFor);
  const msgValue = msgValueN;

  if (!caller) {
    [caller] = await viem.getWalletClients();
  } else {
    caller = await viem.getWalletClient(addressOf(caller));
  }

  const callParams = [
    [rewardsFor],
    { value: msgValue, account: caller.account },
  ] as [[Address], { value: bigint; account: typeof caller.account }];

  if (revertMsg) {
    await expect(referralCenter.write.addRewards(...callParams)).rejectedWith(
      revertMsg,
    );
    return;
  }

  const userInfoBefore = await referralCenter.read.userRewardInfo([rewardsFor]);

  const hash = await referralCenter.write.addRewards(...callParams);
  const publicClient = await viem.getPublicClient();
  const receipt = await publicClient.waitForTransactionReceipt({ hash });
  const events = parseEventLogs({
    abi: referralCenter.abi,
    logs: receipt.logs,
  });
  const addRewardsEvent = events.find(
    (event) =>
      event.eventName === 'AddRewards' &&
      event.args.user === rewardsFor &&
      event.args.amount === msgValue,
  );
  expect(addRewardsEvent).not.eq(undefined);

  const userInfoAfter = await referralCenter.read.userRewardInfo([rewardsFor]);

  expect(userInfoAfter[0]).eq(userInfoBefore[0]);
  expect(userInfoAfter[1]).eq(userInfoBefore[1] + msgValue);
  expect(userInfoAfter[2]).eq(userInfoBefore[2]);
};

export const claimRewards = async ({
  referralCenter,
  caller,
  claimFor,
  revertMsg,
}: {
  referralCenter: Contract<ReferralCenter$Type>;
  claimFor: Address;
  caller?: AccountOrAddress<QueuePool$Type>;
  revertMsg?: string;
}) => {
  claimFor = addressOf(claimFor);

  if (!caller) {
    [caller] = await viem.getWalletClients();
  } else {
    caller = await viem.getWalletClient(addressOf(caller));
  }

  const callParams = [[claimFor], { account: caller.account }] as [
    [Address],
    { account: typeof caller.account },
  ];

  if (revertMsg) {
    await expect(referralCenter.write.claimRewards(...callParams)).rejectedWith(
      revertMsg,
    );
    return;
  }

  const publicClient = await viem.getPublicClient();
  const userInfoBefore = await referralCenter.read.userRewardInfo([claimFor]);
  const userBalanceBefore = await publicClient.getBalance({
    address: claimFor,
  });

  const amountToClaim = userInfoBefore[1] - userInfoBefore[2];
  const hash = await referralCenter.write.claimRewards(...callParams);
  const receipt = await publicClient.waitForTransactionReceipt({ hash });
  const events = parseEventLogs({
    abi: referralCenter.abi,
    logs: receipt.logs,
  });
  const claimRewardsEvent = events.find(
    (event) =>
      event.eventName === 'ClaimRewards' &&
      event.args.user === claimFor &&
      event.args.amount === amountToClaim,
  );
  expect(claimRewardsEvent).not.eq(undefined);

  const userInfoAfter = await referralCenter.read.userRewardInfo([claimFor]);
  const userBalanceAfter = await publicClient.getBalance({ address: claimFor });

  expect(userInfoAfter[0]).eq(userInfoBefore[0]);
  expect(userInfoAfter[1]).eq(userInfoBefore[1]);
  expect(userInfoAfter[2]).eq(userInfoBefore[2] + amountToClaim);
  expect(userBalanceAfter).eq(userBalanceBefore + amountToClaim);
};

export const impersonateSystemAccounts = async ({
  queuePool,
  randomPool,
}: {
  queuePool: Contract<QueuePool$Type>;
  randomPool: Contract<RandomPool$Type>;
}) => {
  await impersonateAccount(queuePool.address);
  await setBalance(queuePool.address, parseUnits('1000', 18));
  await impersonateAccount(randomPool.address);
  await setBalance(randomPool.address, parseUnits('1000', 18));
};

export const stopImpersonatingSystemAccounts = async ({
  queuePool,
  randomPool,
}: {
  queuePool: Contract<QueuePool$Type>;
  randomPool: Contract<RandomPool$Type>;
}) => {
  await stopImpersonatingAccount(queuePool.address);
  await stopImpersonatingAccount(randomPool.address);
};
