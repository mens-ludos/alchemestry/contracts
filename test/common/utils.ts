import { type WalletClient } from '@nomicfoundation/hardhat-viem/types';
import { type Abi, type Address, getAddress } from 'viem';

import { type Contract } from '@/config/types';

export type AccountOrAddress<T extends { abi: Abi } = { abi: [] }> =
  | string
  | WalletClient
  | Contract<T>
  | Address;

export const DEAD_ADDRESS = getAddress(
  '0x000000000000000000000000000000000000dEaD',
);

export const addressOf = <T extends { abi: Abi } = { abi: [] }>(
  account: AccountOrAddress<T>,
): Address => {
  if ((account as WalletClient).account) {
    return (account as WalletClient).account.address;
  }

  if ((account as Contract).address) {
    return (account as Contract).address;
  }

  return getAddress(account as string);
};
