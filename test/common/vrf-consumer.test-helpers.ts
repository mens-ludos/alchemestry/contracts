import {
  impersonateAccount,
  setBalance,
} from '@nomicfoundation/hardhat-toolbox-viem/network-helpers';
import { type WalletClient } from '@nomicfoundation/hardhat-viem/types';
import { expect } from 'chai';
import hre from 'hardhat';
import { parseUnits } from 'viem';

import { type VRFCoordinatorMock$Type } from '@/artifacts/contracts/mock/VRFCoordinatorMock.sol/VRFCoordinatorMock';
import { type VRFConsumer$Type } from '@/artifacts/contracts/vrf/VRFConsumer.sol/VRFConsumer';
import { type Contract } from '@/config/types';

export const requestRandomWords = async ({
  vrfConsumer,
  vrfCoordinator,
  level = 1n,
  words = 1,
  revertMsg,
  caller,
}: {
  vrfConsumer: Contract<VRFConsumer$Type>;
  vrfCoordinator: Contract<VRFCoordinatorMock$Type>;
  words?: number;
  level?: bigint;
  revertMsg?: string;
  caller?: WalletClient;
}) => {
  if (!caller) {
    const poolAddress = await vrfConsumer.read.randomPool();
    await impersonateAccount(poolAddress);
    await setBalance(poolAddress, parseUnits('100', 18));
    caller = await hre.viem.getWalletClient(poolAddress);
  }

  if (revertMsg) {
    await expect(
      vrfConsumer.write.requestRandomWords([words, level], {
        account: caller.account,
      }),
    ).rejectedWith(revertMsg);
    return;
  }

  await expect(
    vrfConsumer.write.requestRandomWords([words, level], {
      account: caller.account,
    }),
  ).not.rejected;

  const requestId = await vrfConsumer.read.lastRequestId();

  // await expect(
  //  vrfCoordinator.write.fulfillRandomWords([
  //    requestId,
  //    Array.from<bigint>({ length: words }).map(() => 1n),
  //  ]),
  // ).not.rejected;

  const hash = await vrfCoordinator.simulate.fulfillRandomWords([
    requestId,
    Array.from<bigint>({ length: words }).map(() => 1n),
  ]);

  console.log(hash);

  const request = await vrfConsumer.read.getRequestStatus([requestId]);

  expect(request.exists).eq(true);
  expect(request.fulfilled).eq(false);
  expect(request.forLevel).eq(level);
  expect(request.randomWordsLength).eq(words);
};
