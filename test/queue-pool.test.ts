import { viem } from 'hardhat';

import { deployWithMockedConsumer } from '@/test/common/fixtures';
import { waitForLevel } from '@/test/common/pools-test-helpers';
import {
  buyTables,
  buyTablesMultTest,
  claimTableRewards,
} from '@/test/common/queue-pool.test-helpers';

describe('QueuePool', function () {
  describe('deployment', () => {
    it('valid deployment', async () => {
      await deployWithMockedConsumer();
    });
  });

  describe('buyTables', () => {
    describe('should pass', () => {
      it('level 1: buy 1 table', async () => {
        const { queuePool, alchemestry } = await deployWithMockedConsumer();
        await waitForLevel({ alchemestry });
        await buyTables({ queuePool });
      });

      it('level 1: buy 1 table with referral that already bought a table at current level', async () => {
        const { queuePool, alchemestry, regularAccounts } =
          await deployWithMockedConsumer();
        const referral = regularAccounts[0];

        await waitForLevel({ alchemestry });
        await buyTables({
          queuePool: await viem.getContractAt('QueuePool', queuePool.address, {
            client: { wallet: referral },
          }),
        });

        await buyTables({ queuePool, referral: referral.account.address });
      });

      it('level 1: buy 1 table with referral that does`nt have any table at current level', async () => {
        const { queuePool, alchemestry, regularAccounts } =
          await deployWithMockedConsumer();
        const referral = regularAccounts[0];
        await waitForLevel({ alchemestry });
        await buyTables({ queuePool, referral: referral.account.address });
      });

      it('level 1: buy multiple tables in multiple transactions from same address', async () => {
        const { queuePool, alchemestry } = await deployWithMockedConsumer();
        await waitForLevel({ alchemestry });
        await buyTables({ queuePool });
        await buyTables({ queuePool });
      });

      it('level 2: buy 1 table when have 1 table for lvl 1', async () => {
        const { queuePool, alchemestry } = await deployWithMockedConsumer();
        let level = 1n;
        await waitForLevel({ alchemestry, level });
        await buyTables({ queuePool, level });
        level++;
        await waitForLevel({ alchemestry, level });
        await buyTables({ queuePool, level });
      });

      describe('fullfil', () => {
        it('level 1: user1 -> user2 -> user2', async () => {
          const { queuePool, alchemestry, regularAccounts } =
            await deployWithMockedConsumer();

          const [user1, user2] = regularAccounts;

          await waitForLevel({ alchemestry });

          await buyTablesMultTest({
            queuePool,
            buyInfos: [{ buyer: user1 }, { buyer: user2 }, { buyer: user2 }],
          });

          await claimTableRewards({
            queuePool,
            owner: user1.account.address,
            expectedFilledTables: 1n,
          });
        });

        it('level 1: user1 -> user2 -> user1 -> user2:2 -> user3:2', async () => {
          const { queuePool, alchemestry, regularAccounts } =
            await deployWithMockedConsumer();

          const [user1, user2, user3] = regularAccounts;

          await waitForLevel({ alchemestry });

          console.log({
            user1: user1.account.address,
            user2: user2.account.address,
            user3: user3.account.address,
          });

          await buyTablesMultTest({
            queuePool,
            buyInfos: [
              { buyer: user1 }, // 1
              { buyer: user2 }, // 1
              { buyer: user1 }, // 1
              { buyer: user2, amount: 2n }, // 0
              { buyer: user3, amount: 2n }, // 0
            ],
          });

          await claimTableRewards({
            queuePool,
            owner: user1.account.address,
            expectedFilledTables: 2n,
          });
          await claimTableRewards({
            queuePool,
            owner: user2.account.address,
            expectedFilledTables: 1n,
          });
          await claimTableRewards({
            queuePool,
            owner: user3.account.address,
            expectedFilledTables: 0n,
          });
        });

        it('level 1: user1:2 -> user2:3 -> user3:10 -> user2:5 -> user3:10', async () => {
          const { queuePool, alchemestry, regularAccounts } =
            await deployWithMockedConsumer();

          const [user1, user2, user3] = regularAccounts;

          await waitForLevel({ alchemestry });

          await buyTablesMultTest({
            queuePool,
            buyInfos: [
              { buyer: user1, amount: 2n }, // 1
              { buyer: user2, amount: 3n }, // 1
              { buyer: user3, amount: 10n }, // 1
              { buyer: user2, amount: 5n }, // 0
              { buyer: user3, amount: 10n }, // 0
              { buyer: user1, amount: 5n }, // 1
            ],
          });

          await claimTableRewards({
            queuePool,
            owner: user1.account.address,
            expectedFilledTables: 2n,
          });
          await claimTableRewards({
            queuePool,
            owner: user2.account.address,
            expectedFilledTables: 8n,
          });
          await claimTableRewards({
            queuePool,
            owner: user3.account.address,
            expectedFilledTables: 5n,
          });
        });

        it('level 1: gaps user1:10 -> user2:3 -> user1:1 -> user2:2 -> user1:1 -> user2:3 -> user3:10 -> user3:10', async () => {
          const { queuePool, alchemestry, regularAccounts } =
            await deployWithMockedConsumer();

          const [user1, user2, user3] = regularAccounts;

          await waitForLevel({ alchemestry });

          await buyTablesMultTest({
            queuePool,
            buyInfos: [
              { buyer: user1, amount: 10n },
              { buyer: user2, amount: 3n },
              { buyer: user1, amount: 1n },
              { buyer: user2, amount: 2n },
              { buyer: user1, amount: 1n },
              { buyer: user2, amount: 3n },
              { buyer: user3, amount: 10n },
              { buyer: user3, amount: 10n },
              { buyer: user3, amount: 10n },
              { buyer: user3, amount: 10n },
            ],
          });

          await claimTableRewards({
            queuePool,
            owner: user1.account.address,
            expectedFilledTables: 12n,
          });
          await claimTableRewards({
            queuePool,
            owner: user2.account.address,
            expectedFilledTables: 8n,
          });
          await claimTableRewards({
            queuePool,
            owner: user3.account.address,
            expectedFilledTables: 0n,
          });
        });
      });
    });

    describe('should fail', () => {
      it('level 1: buy 1 table when game is not yet started', async () => {
        const { queuePool } = await deployWithMockedConsumer();
        await buyTables({ queuePool, revertMsg: 'Queue: pool is not started' });
      });

      it('level 2: buy 1 table when don`t have any tables for lvl 1', async () => {
        const { queuePool, alchemestry } = await deployWithMockedConsumer();
        await waitForLevel({ alchemestry, level: 2n });
        await buyTables({
          queuePool,
          level: 2n,
          revertMsg: 'Queue: insufficient level tables',
        });
      });
    });
  });
});
