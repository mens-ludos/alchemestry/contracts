import { time } from '@nomicfoundation/hardhat-toolbox-viem/network-helpers';
import { viem } from 'hardhat';

import { deployWithMockedConsumer } from '@/test/common/fixtures';
import { waitForLevel } from '@/test/common/pools-test-helpers';
import { buyTables } from '@/test/common/random-pool.test-helpers';

describe('RandomPool', function () {
  describe('deployment', () => {
    it('valid deployment', async () => {
      await deployWithMockedConsumer();
    });
  });

  describe('buyTables', () => {
    describe('should pass', () => {
      it('level 1: buy 1 table', async () => {
        const { randomPool, alchemestry, referralCenter } =
          await deployWithMockedConsumer();
        await waitForLevel({ alchemestry });
        await buyTables({ randomPool, referralCenter });
      });

      it('level 1: buy 1 table with referral that already bought a table at current level', async () => {
        const { randomPool, alchemestry, regularAccounts, referralCenter } =
          await deployWithMockedConsumer();
        const referral = regularAccounts[0];

        await waitForLevel({ alchemestry });
        await buyTables({
          randomPool: await viem.getContractAt(
            'RandomPool',
            randomPool.address,
            {
              client: { wallet: referral },
            },
          ),
          referralCenter,
        });

        await buyTables({
          randomPool,
          referral: referral.account.address,
          referralCenter,
        });
      });

      it('level 1: buy 1 table with referral that does`nt have any table at current level', async () => {
        const { randomPool, alchemestry, regularAccounts, referralCenter } =
          await deployWithMockedConsumer();
        const referral = regularAccounts[0];
        await waitForLevel({ alchemestry });
        await buyTables({
          randomPool,
          referral: referral.account.address,
          referralCenter,
        });
      });

      it('level 1: buy multiple tables in multiple transactions from same address', async () => {
        const { randomPool, alchemestry, referralCenter } =
          await deployWithMockedConsumer();
        await waitForLevel({ alchemestry });
        await buyTables({ randomPool, referralCenter });
        await buyTables({ randomPool, referralCenter });
      });

      it('level 2: buy 1 table when have 1 table for lvl 1', async () => {
        const { randomPool, alchemestry, referralCenter } =
          await deployWithMockedConsumer();
        let level = 1n;
        await waitForLevel({ alchemestry, level });
        await buyTables({ randomPool, level, referralCenter });
        level++;
        await waitForLevel({ alchemestry, level });
        await buyTables({ randomPool, level, referralCenter });
      });
    });

    describe('should fail', () => {
      it('level 1: buy 1 table when game is not yet started', async () => {
        const { randomPool, referralCenter } = await deployWithMockedConsumer();
        await buyTables({
          randomPool,
          revertMsg: 'Random: pool not started/over',
          referralCenter,
        });
      });

      it('level 3: when game is over', async () => {
        const { randomPool, referralCenter } = await deployWithMockedConsumer();
        const currTime = await time.latest();
        await time.setNextBlockTimestamp(currTime * 2);
        await buyTables({
          randomPool,
          level: 1n,
          revertMsg: 'Random: pool not started/over',
          referralCenter,
        });
      });

      it('level 2: buy 1 table when don`t have any tables for lvl 1 in queue pool', async () => {
        const { randomPool, alchemestry, referralCenter } =
          await deployWithMockedConsumer();
        await waitForLevel({ alchemestry, level: 2n });
        await buyTables({
          randomPool,
          level: 2n,
          revertMsg: 'Random: insufficient level tables',
          referralCenter,
        });
      });

      it('level 2: buy lvl1 table', async () => {
        const { randomPool, alchemestry, referralCenter } =
          await deployWithMockedConsumer();
        await waitForLevel({ alchemestry, level: 2n });
        await buyTables({
          randomPool,
          level: 1n,
          revertMsg: 'Random: invalid level',
          referralCenter,
        });
      });
    });
  });
});
