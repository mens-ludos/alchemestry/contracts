import { setBalance } from '@nomicfoundation/hardhat-toolbox-viem/network-helpers';
import { expect } from 'chai';
import { parseUnits, zeroAddress, getAddress } from 'viem';

import { deployWithMockedConsumer } from '@/test/common/fixtures';
import {
  addRewards,
  claimRewards,
  createUserIfNotExists,
  impersonateSystemAccounts,
  stopImpersonatingSystemAccounts,
} from '@/test/common/referral-center.test-helpers';
import { DEAD_ADDRESS } from '@/test/common/utils';

describe('ReferralCenter', function () {
  it('deployment', async () => {
    const { referralCenter, alchemestry, queuePool, randomPool } =
      await deployWithMockedConsumer();

    expect(await referralCenter.read.alchemestry()).eq(
      getAddress(alchemestry.address),
    );
    expect(await referralCenter.read.queuePool()).eq(
      getAddress(queuePool.address),
    );
    expect(await referralCenter.read.randomPool()).eq(
      getAddress(randomPool.address),
    );

    expect(await referralCenter.read.NO_REFERRER_ADDRESS()).eq(DEAD_ADDRESS);
  });

  describe('createUserIfNotExists', () => {
    it('if user is a valid address and referrer is address(0)', async () => {
      const { regularAccounts, referralCenter, queuePool, randomPool } =
        await deployWithMockedConsumer();
      await impersonateSystemAccounts({ queuePool, randomPool });

      await createUserIfNotExists({
        referralCenter,
        user: regularAccounts[0].account.address,
        caller: queuePool,
      });

      await stopImpersonatingSystemAccounts({ queuePool, randomPool });
    });

    it('if user is a valid address but its already registered and it doesnt have referrer', async () => {
      const { regularAccounts, referralCenter, queuePool, randomPool } =
        await deployWithMockedConsumer();
      await impersonateSystemAccounts({ queuePool, randomPool });

      await createUserIfNotExists({
        referralCenter,
        user: regularAccounts[0].account.address,
        caller: queuePool,
      });

      await createUserIfNotExists({
        referralCenter,
        user: regularAccounts[0].account.address,
        caller: queuePool,
      });
      await stopImpersonatingSystemAccounts({ queuePool, randomPool });
    });

    it('if user is a valid address and referrer is already registered', async () => {
      const { regularAccounts, referralCenter, queuePool, randomPool } =
        await deployWithMockedConsumer();
      await impersonateSystemAccounts({ queuePool, randomPool });

      await createUserIfNotExists({
        referralCenter,
        user: regularAccounts[1].account.address,
        caller: queuePool,
      });

      await createUserIfNotExists({
        referralCenter,
        user: regularAccounts[0].account.address,
        caller: queuePool,
        referrer: regularAccounts[1].account.address,
      });

      await stopImpersonatingSystemAccounts({ queuePool, randomPool });
    });

    it('if user is already registered but referrer is not registered ', async () => {
      const { regularAccounts, referralCenter, queuePool, randomPool } =
        await deployWithMockedConsumer();
      await impersonateSystemAccounts({ queuePool, randomPool });

      await createUserIfNotExists({
        referralCenter,
        user: regularAccounts[0].account.address,
        caller: queuePool,
      });

      await createUserIfNotExists({
        referralCenter,
        user: regularAccounts[0].account.address,
        caller: queuePool,
        referrer: regularAccounts[1].account.address,
      });

      await stopImpersonatingSystemAccounts({ queuePool, randomPool });
    });

    it('should fail: if user is an invalid address', async () => {
      const { referralCenter, queuePool, randomPool } =
        await deployWithMockedConsumer();
      await impersonateSystemAccounts({ queuePool, randomPool });

      await createUserIfNotExists({
        referralCenter,
        user: zeroAddress,
        caller: queuePool,
        revertMsg: 'RC: invalid user',
      });

      await stopImpersonatingSystemAccounts({ queuePool, randomPool });
    });

    it('should fail: if caller is not queue or random pool', async () => {
      const { regularAccounts, referralCenter } =
        await deployWithMockedConsumer();

      await createUserIfNotExists({
        referralCenter,
        user: regularAccounts[0].account.address,
        revertMsg: 'RC: not authorized',
      });
    });

    it('should fail: if user is a valid address but referrer is not registered', async () => {
      const { regularAccounts, referralCenter, queuePool, randomPool } =
        await deployWithMockedConsumer();
      await impersonateSystemAccounts({ queuePool, randomPool });

      await createUserIfNotExists({
        referralCenter,
        caller: queuePool,
        user: regularAccounts[0].account.address,
        referrer: regularAccounts[1].account.address,
        revertMsg: 'RC: r not exists',
      });

      await stopImpersonatingSystemAccounts({ queuePool, randomPool });
    });
  });

  describe('addRewards', () => {
    it('if rewardsFor is exists and the value is valid', async () => {
      const { regularAccounts, referralCenter, queuePool, randomPool } =
        await deployWithMockedConsumer();
      await impersonateSystemAccounts({ queuePool, randomPool });

      await createUserIfNotExists({
        referralCenter,
        caller: queuePool,
        user: regularAccounts[0].account.address,
      });

      await setBalance(queuePool.address, parseUnits('1', 18));

      await addRewards({
        referralCenter,
        rewardsFor: regularAccounts[0].account.address,
        msgValueN: 1n,
        caller: queuePool,
      });

      await stopImpersonatingSystemAccounts({ queuePool, randomPool });
    });

    it('if rewardsFor is exists and the value is valid and call addRewards few times', async () => {
      const { regularAccounts, referralCenter, queuePool, randomPool } =
        await deployWithMockedConsumer();
      await impersonateSystemAccounts({ queuePool, randomPool });

      await createUserIfNotExists({
        referralCenter,
        caller: queuePool,
        user: regularAccounts[0].account.address,
      });

      await setBalance(queuePool.address, parseUnits('3', 18));

      await addRewards({
        referralCenter,
        rewardsFor: regularAccounts[0].account.address,
        msgValueN: 1n,
        caller: queuePool,
      });

      await addRewards({
        referralCenter,
        rewardsFor: regularAccounts[0].account.address,
        msgValueN: 1n,
        caller: queuePool,
      });

      await addRewards({
        referralCenter,
        rewardsFor: regularAccounts[0].account.address,
        msgValueN: 1n,
        caller: queuePool,
      });

      await stopImpersonatingSystemAccounts({ queuePool, randomPool });
    });

    it('should fail: if rewardsFor is an invalid address', async () => {
      const { regularAccounts, referralCenter, queuePool, randomPool } =
        await deployWithMockedConsumer();
      await impersonateSystemAccounts({ queuePool, randomPool });

      await createUserIfNotExists({
        referralCenter,
        caller: queuePool,
        user: regularAccounts[0].account.address,
      });

      await setBalance(queuePool.address, parseUnits('1', 18));

      await addRewards({
        referralCenter,
        rewardsFor: zeroAddress,
        msgValueN: 1n,
        caller: queuePool,
        revertMsg: 'RC: user not exists',
      });

      await stopImpersonatingSystemAccounts({ queuePool, randomPool });
    });

    it('should fail: if msg.value is 0', async () => {
      const { regularAccounts, referralCenter, queuePool, randomPool } =
        await deployWithMockedConsumer();
      await impersonateSystemAccounts({ queuePool, randomPool });

      await createUserIfNotExists({
        referralCenter,
        caller: queuePool,
        user: regularAccounts[0].account.address,
      });

      await addRewards({
        referralCenter,
        rewardsFor: zeroAddress,
        caller: queuePool,
        revertMsg: 'RC: nothing to add',
      });

      await stopImpersonatingSystemAccounts({ queuePool, randomPool });
    });

    it('should fail: if caller is not authorized', async () => {
      const { regularAccounts, referralCenter, queuePool, randomPool } =
        await deployWithMockedConsumer();
      await impersonateSystemAccounts({ queuePool, randomPool });

      await createUserIfNotExists({
        referralCenter,
        caller: queuePool,
        user: regularAccounts[0].account.address,
      });

      await addRewards({
        referralCenter,
        rewardsFor: zeroAddress,
        revertMsg: 'RC: not authorized',
      });

      await stopImpersonatingSystemAccounts({ queuePool, randomPool });
    });
  });

  describe('claimRewards', () => {
    it('if claimFor is exists and has something to claim', async () => {
      const { regularAccounts, referralCenter, queuePool, randomPool } =
        await deployWithMockedConsumer();
      await impersonateSystemAccounts({ queuePool, randomPool });

      await createUserIfNotExists({
        referralCenter,
        caller: queuePool,
        user: regularAccounts[0].account.address,
      });

      await setBalance(queuePool.address, parseUnits('1', 18));

      await addRewards({
        referralCenter,
        rewardsFor: regularAccounts[0].account.address,
        msgValueN: 1n,
        caller: queuePool,
      });

      await claimRewards({
        referralCenter,
        claimFor: regularAccounts[0].account.address,
        caller: queuePool,
      });

      await stopImpersonatingSystemAccounts({ queuePool, randomPool });
    });

    it('should fail: if claimFor is not exists', async () => {
      const { regularAccounts, referralCenter, queuePool, randomPool } =
        await deployWithMockedConsumer();
      await impersonateSystemAccounts({ queuePool, randomPool });

      await claimRewards({
        referralCenter,
        claimFor: regularAccounts[0].account.address,
        caller: queuePool,
        revertMsg: 'RC: user not exists',
      });

      await stopImpersonatingSystemAccounts({ queuePool, randomPool });
    });

    it('should fail: if nothing to claim', async () => {
      const { regularAccounts, referralCenter, queuePool, randomPool } =
        await deployWithMockedConsumer();
      await impersonateSystemAccounts({ queuePool, randomPool });

      await createUserIfNotExists({
        referralCenter,
        caller: queuePool,
        user: regularAccounts[0].account.address,
      });

      await claimRewards({
        referralCenter,
        claimFor: regularAccounts[0].account.address,
        revertMsg: 'RC: nothing to claim',
      });

      await stopImpersonatingSystemAccounts({ queuePool, randomPool });
    });
  });
});
