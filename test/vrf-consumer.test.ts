import { deployWithMockedConsumer } from '@/test/common/fixtures';
import { requestRandomWords } from '@/test/common/vrf-consumer.test-helpers';

describe.skip('VRFConsumer', function () {
  describe('deployment', () => {
    it('valid deployment', async () => {
      await deployWithMockedConsumer();
    });
  });

  describe('requestRandomWords', () => {
    describe('should pass', () => {
      it('Request 1 word from coordinator', async () => {
        const { vrfConsumer, vrfCoordinator } =
          await deployWithMockedConsumer();
        await requestRandomWords({ vrfConsumer, vrfCoordinator });
      });
    });

    describe('should fail', () => {});
  });
});
